/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

(function ($) {

  Drupal.behaviors.cookiebar = {
    attach: function (context, settings) {
      // test cookies
      initCookiebar(settings);
    }
  };
  
  function initCookiebar(settings) {
    // plugin available?
    if (typeof $.cookie == 'undefined') {
      return;
    }
    
    // cookie mechanism works?
    $.cookie('dough', 'bake');
    if ($.cookie('dough') != 'bake') {
      return;
    }
    $.cookie('dough', null);
    
    // cookiebar provided by plugin?
    if (typeof settings.cookiebar.html == 'undefined') {
      return;
    }
    
    $('body').append(settings.cookiebar.html);

    $('#cb-accept').click(function(event){
      if (event.preventDefault) {
        event.preventDefault();
      }
      
      var cbVal = Drupal.settings.cookiebar.cookie + ' since ' + (new Date()).toUTCString();
      var cbExpire = new Date(Drupal.settings.cookiebar.expires);
      var cbSetting = {
        'expires': cbExpire
      };
      
      $.cookie(Drupal.settings.cookiebar.cookie, cbVal, cbSetting);
      location.reload();

      return false;
    });
    
    $('#cb-deny').click(function(event){
      if (event.preventDefault) {
        event.preventDefault();
      }
      
      var cbExpire = new Date(Drupal.settings.cookiebar.expires);
      var cbSetting = {
        'expires': cbExpire
      };
      
      $.cookie(Drupal.settings.cookiebar.cookie, Drupal.settings.cookiebar.deny, cbSetting);
      location.reload();

      return false;
    });
    
    $('#cb-change').click(function(event){
      $('#cookiebar').removeClass('hidden');
    });
  }
  
})(jQuery);