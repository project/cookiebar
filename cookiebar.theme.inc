<?php

/**
 * Conditional translate
 * 
 * @param string $msg
 * @param bool $translate
 * @return string
 */
function _ct($msg, $translate=TRUE) {
  if ($translate) {
    return t($msg);
  }
  
  return $msg;
}

function theme_cookiebar(&$variables) {
  
  $message   = variable_get('cookiebar_message', 'This site uses third party cookies.');
  $message2  = variable_get('cookiebar_message_allow', 'Thanks for allowing us to use third party cookies.');
  $allow     = variable_get('cookiebar_allow', 'No problem');
  $deny      = variable_get('cookiebar_deny', 'I don\'t want it');
  $tr        = variable_get('cookiebar_translate', '1');
  $readmore  = variable_get('cookiebar_readmore');
  
  
  if (cookiebar_get_status()) {
    // enabled
    $message = $message2;
  }
  if (cookiebar_get_cookiestatus()) {
    $class = ' hidden';
  } else {
    $class = '';
  }
  
  $html = "<div id=\"cookiebar\" class=\"{$class}\">";
  $html .= '<div class="wrapper">';
  $html .= '<div class="content">';
  
  if (!empty($readmore)) {
    $l = l(_ct('Read more', $tr), $readmore);
  } else {
    $l = '';
  }
  
  $html .= '<div class="message">' . check_markup(_ct($message, $tr)) . $l . '</div>';
  
  $html .= '<div class="allow-deny">';
  $html .= '<input type="button" id="cb-accept" value="' . check_plain(_ct($allow, $tr)). '">';
  
  if (!empty($deny)) {
    $html .= '<input type="button" id="cb-deny" value="' . check_plain(_ct($deny, $tr)). '">';
  }
  $html .= '</div>';

  $html .= '</div>'; // </content>
  $html .= '</div>'; // </wrapper>
  $html .= '</div>'; // </cookiebar>
  
  return $html;
}        