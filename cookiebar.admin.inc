<?php

/**
 * Build a configuration form for the module
 * 
 * @param type $form
 * @param type $form_state
 * @param type $param
 * @return array
 */
function cookiebar_configuration_form($form, &$form_state, $param = NULL) {
  $settings = variable_get('cookiebar_options');
  
  if (!is_array($settings)) {
    drupal_set_message(t('No settings defined'));
  }
  
  foreach($settings as $key => $setting) {
    if (!empty($setting)) {
      $defaults[] = $key;
    }
  }
  
  $options = array_keys($settings);
  
  $form = array(
    'select scripts' => array(
      '#type' => 'fieldset',
      '#title' => t('Cookiebar settings'),
      'cookiebar_message' => array(
        '#type' => 'textarea',
        '#title' => t('Message'),
        '#description' => t('Explain what the risks are for visiting this site'),
        '#default_value' => variable_get('cookiebar_message', 'This site uses third party cookies.'),
      ),
      'cookiebar_message_allow' => array(
        '#type' => 'textarea',
        '#title' => t('Allowed message'),
        '#description' => t('Message to show if the user allows cookies at the moment.'),
        '#default_value' => variable_get('cookiebar_message_allow', 'Thanks for allowing us to use third party cookies.'),
      ),
      'cookiebar_allow' => array(
        '#type' => 'textfield',
        '#title' => t('Allow button'),
        '#description' => t('Enter the text for the "Allow" button'),
        '#default_value' => variable_get('cookiebar_allow', 'No problem'),
      ),
      'cookiebar_deny' => array(
        '#type' => 'textfield',
        '#title' => t('Deny button'),
        '#description' => t('Enter the text for the "Deny" button'),
        '#default_value' => variable_get('cookiebar_deny', 'I don\'t want it'),
      ),
      'cookiebar_translate' => array(
        '#type' => 'checkbox',
        '#title' => t('Translate'),
        '#description' => t('Use the drupal translation facilities'),
        '#default_value' => variable_get('cookiebar_translate', 1),
      ),
      'cookiebar_readmore' => array(
        '#type' => 'textfield',
        '#title' => t('Read more'),
        '#description' => t('Provide a URL to an article to read more about it.'),
        '#default_value' => variable_get('cookiebar_readmore'),
      ),
      'cookiebar_options' => array(
        '#type' => 'checkboxes',
        '#title' => t('Select optional javascript files'),
        '#description' => t('Select the javascript files to disable when
          cookies are not allowed. This list is build and updated each page
          visit, so if the javascript instance that you are looking for is not
          there it might help to visit a page where the javascript is used.'),
        '#options' => drupal_map_assoc($options),
        '#default_value' => drupal_map_assoc($defaults),
      ),
    ),
  );
  
  $form['actions']['clear'] = array(
    '#type' => 'submit',
    '#value' => t('Clear'),
    '#submit' => array('cookiebar_configuration_form_clear'),
    '#weight' => 100,
  );
  
  return system_settings_form($form);
}

/**
 * Throw away the cash of javascript files.
 * 
 * @param type $form
 * @param type $form_state
 */
function cookiebar_configuration_form_clear($form, &$form_state) {
  variable_del('cookiebar_options');
}